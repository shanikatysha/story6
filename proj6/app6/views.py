from django.shortcuts import render
from django.http import HttpResponse
from .models import DaftarPeserta

# Create your views here.
def fungsi_formulir(request):
    #return HttpResponse("sesuatu") #keluar sesuatu di local host /formulir
    response = {}
    return render (request,"formulir.html",response) #bikin templatenya

def fungsi_hasil(request):
    if request.method == 'POST' : #masukin hasil POST , tp belom ambil kembali buat ditampilin di hasil
        DaftarPeserta.objects.create(nama=request.POST['nama'], kegiatan=request.POST['kegiatan']) #import DaftarPeserta
    isi_daftar_peserta = DaftarPeserta.objects.all()
    response = {
        'isi_daftar_peserta' : isi_daftar_peserta
    } #abis ini loop ke dalam html
    return render (request,"hasil.html",response) #bikin templatenya

