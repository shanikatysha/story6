from django.test import TestCase, Client
from .models import DaftarPeserta

# Create your tests here.
class Story6Test(TestCase):
    
    def test_url_formulir(self):
        response = Client().get('/formulir/')
        self.assertEquals(200,response.status_code) #formulir blm diimplementasi -> ayo ke urls.py

    def test_nama_templates_formulir(self):
        response = Client().get('/formulir/')
        self.assertTemplateUsed(response, 'formulir.html') #nama html haru formulir.html

    def test_isi_view_html_formulir(self):
        response = Client().get('/formulir/')
        html_response = response.content.decode('utf8') #byte to string
        #self.assertIn("Kegiatan 1", html_response) #harus ada dalam html
        #self.assertIn("Tambah Peserta", html_response)
        self.assertIn('<input id="nama" type="text" name="nama" placeholder="Masukkan nama peserta">', html_response)
        self.assertIn('<input id="kegiatan" type="text" name="kegiatan" placeholder="Masukkan jenis kegiatan">', html_response)
        self.assertIn('<button id="kirim" type="submit" name="kirim">Tambah peserta</button>', html_response)
        self.assertIn('<form action="/hasil/" method=POST>',html_response)

    def test_url_hasil(self):
        response = Client().get('/hasil/')
        self.assertEquals(200,response.status_code) #formulir blm diimplementasi -> ayo ke urls.py

    def test_nama_templates_hasil(self):
        response = Client().get('/hasil/')
        self.assertTemplateUsed(response, 'hasil.html') #nama html haru formulir.html

    def test_isi_view_html_hasil(self):
        response = Client().get('/hasil/')
        html_response = response.content.decode('utf8') #byte to string
        self.assertIn("Daftar peserta dan kegiatan", html_response) #harus ada dalam html
        self.assertIn("Nama peserta", html_response)
        self.assertIn("Kegiatan", html_response)
        self.assertIn("<a href='/formulir'>Kembali ke halaman utama</a>", html_response)

    def test_models_daftar(self):
        DaftarPeserta.objects.create(nama="nama orang", kegiatan="kegiatannya") #unexpected keyword argument -> di model gaada nama
        jumlah_peserta = DaftarPeserta.objects.all().count()
        self.assertEquals(jumlah_peserta,1) #artinya tiap creat objek, jml nya harus 1
    
    def test_kirim_post(self): #tes method post
        arg = { #expect html_response isinya ini nama orang dan kegiatannya
            'nama':'nama orang',
            'kegiatan':'kegiatannya'
        }
        response = Client().post('/hasil/',arg) #bakal faile karena di views.py belom handle post
        html_response = response.content.decode('utf8')
        self.assertIn("nama orang", html_response)
        self.assertIn("kegiatannya", html_response)
